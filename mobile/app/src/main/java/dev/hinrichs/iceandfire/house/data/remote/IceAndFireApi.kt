package dev.hinrichs.iceandfire.house.data.remote

import retrofit2.http.GET
import retrofit2.http.Path

interface IceAndFireApi {
    @GET("api/houses")
    suspend fun fetchHouses(): List<HouseDto>

    @GET("api/houses/{houseId}")
    suspend fun fetchHouse(@Path("houseId") houseId: String): HouseDto

    @GET("api/characters")
    suspend fun fetchCharacters(): List<CharacterDto>
}
