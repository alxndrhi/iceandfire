package dev.hinrichs.iceandfire.house.presentation.house_list

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.hinrichs.iceandfire.house.data.Resource
import dev.hinrichs.iceandfire.house.domain.use_case.GetAllHousesUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class HouseListViewModel @Inject constructor(
    private val getAllHousesUseCase: GetAllHousesUseCase
) : ViewModel() {
    private val _state = mutableStateOf(HouseListState())
    val state: State<HouseListState> = _state

    init {
        getHouses()
    }

    private fun getHouses() {
        getAllHousesUseCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = HouseListState(data = result.data ?: emptyList())
                }

                is Resource.Error -> {
                    _state.value =
                        HouseListState(error = result.message ?: "An unexpected error occurred")
                }

                is Resource.Loading -> {
                    _state.value = HouseListState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}
