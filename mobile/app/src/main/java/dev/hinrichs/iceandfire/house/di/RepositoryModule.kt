package dev.hinrichs.iceandfire.house.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dev.hinrichs.iceandfire.house.data.repository.CharacterRepositoryImpl
import dev.hinrichs.iceandfire.house.data.repository.HouseRepositoryImpl
import dev.hinrichs.iceandfire.house.domain.repository.CharacterRepository
import dev.hinrichs.iceandfire.house.domain.repository.HouseRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindHouseRepository(
        houseRepositoryImpl: HouseRepositoryImpl
    ): HouseRepository

    @Binds
    @Singleton
    abstract fun bindCharacterRepository(
        characterRepositoryImpl: CharacterRepositoryImpl
    ): CharacterRepository
}
