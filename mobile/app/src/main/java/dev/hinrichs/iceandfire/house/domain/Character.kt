package dev.hinrichs.iceandfire.house.domain

data class Character(
    val url: String,
    val name: String,
    val aliases: List<String> = emptyList(),
)
