package dev.hinrichs.iceandfire.house.data.remote

data class CharacterDto(
    val url: String,
    val name: String,
    val aliases: List<String>,
)
