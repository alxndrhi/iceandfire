package dev.hinrichs.iceandfire.house.domain.use_case

import dev.hinrichs.iceandfire.house.data.Resource
import dev.hinrichs.iceandfire.house.domain.House
import dev.hinrichs.iceandfire.house.domain.repository.HouseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import javax.inject.Inject

class GetAllHousesUseCase @Inject constructor(
    private val houseRepository: HouseRepository
) {

    operator fun invoke(): Flow<Resource<List<House>>> = flow {
        try {
            emit(Resource.Loading())
            val houses = houseRepository.fetchHouses()
            emit(Resource.Success(data = houses))
        } catch (e: HttpException) {
            emit(Resource.Error(message = e.message ?: "received unexpected response from server"))
        } catch (e: Exception) {
            emit(Resource.Error(message = e.message ?: "unknown error"))
        }
    }
}
