package dev.hinrichs.iceandfire.house.presentation.house_detail

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.hinrichs.iceandfire.house.data.Resource
import dev.hinrichs.iceandfire.house.domain.use_case.GetHouseDetailUseCase
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class HouseDetailViewModel @Inject constructor(
    private val getHouseDetailUseCase: GetHouseDetailUseCase,
    savedStateHandle: SavedStateHandle,
) : ViewModel() {
    private val _state = mutableStateOf(HouseDetailState())
    val state: State<HouseDetailState> = _state

    init {
        savedStateHandle.get<String>("houseId")?.let { id ->
            getHouse(id)
        }
    }

    private fun getHouse(id: String) {
        getHouseDetailUseCase(id).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = HouseDetailState(data = result.data)
                }

                is Resource.Error -> {
                    _state.value = HouseDetailState(
                        error = result.message ?: "An unexpected error occurred"
                    )
                }

                is Resource.Loading -> {
                    _state.value = HouseDetailState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}
