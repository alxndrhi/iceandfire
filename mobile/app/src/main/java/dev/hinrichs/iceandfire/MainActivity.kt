package dev.hinrichs.iceandfire

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import dagger.hilt.android.AndroidEntryPoint
import dev.hinrichs.iceandfire.house.presentation.Screen
import dev.hinrichs.iceandfire.house.presentation.house_detail.HouseDetailScreen
import dev.hinrichs.iceandfire.house.presentation.house_list.HouseListScreen
import dev.hinrichs.iceandfire.ui.theme.IceAndFireTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            IceAndFireTheme {
                Surface(
                    modifier = Modifier.fillMaxSize()
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.HouseList.route,
                        modifier = Modifier.padding(16.dp)
                    ) {
                        composable(route = Screen.HouseList.route) {
                            HouseListScreen(navController = navController)
                        }
                        composable(
                            route = Screen.HouseDetail.route + "/{houseId}",
                            arguments = listOf(
                                navArgument("houseId") {
                                    type = NavType.StringType
                                }
                            )
                        ) {
                            HouseDetailScreen()
                        }
                    }
                }
            }
        }
    }
}
