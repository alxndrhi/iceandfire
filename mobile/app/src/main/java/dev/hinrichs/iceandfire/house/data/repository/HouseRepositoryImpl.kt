package dev.hinrichs.iceandfire.house.data.repository

import dev.hinrichs.iceandfire.house.data.mapper.toDomain
import dev.hinrichs.iceandfire.house.data.remote.IceAndFireApi
import dev.hinrichs.iceandfire.house.domain.House
import dev.hinrichs.iceandfire.house.domain.repository.HouseRepository
import javax.inject.Inject

class HouseRepositoryImpl @Inject constructor(
    private val iceAndFireApi: IceAndFireApi,
) : HouseRepository {
    override suspend fun fetchHouses(): List<House> {
        return iceAndFireApi.fetchHouses().map { it.toDomain() }
    }

    override suspend fun fetchHouse(houseId: String): House {
        return iceAndFireApi.fetchHouse(houseId).toDomain()
    }
}
