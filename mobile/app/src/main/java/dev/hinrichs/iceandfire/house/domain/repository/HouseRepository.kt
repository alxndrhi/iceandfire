package dev.hinrichs.iceandfire.house.domain.repository

import dev.hinrichs.iceandfire.house.domain.House

interface HouseRepository {
    suspend fun fetchHouses(): List<House>
    suspend fun fetchHouse(houseId: String): House
}
