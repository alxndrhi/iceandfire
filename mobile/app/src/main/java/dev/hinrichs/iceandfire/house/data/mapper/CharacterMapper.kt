package dev.hinrichs.iceandfire.house.data.mapper

import dev.hinrichs.iceandfire.house.data.remote.CharacterDto
import dev.hinrichs.iceandfire.house.domain.Character

fun CharacterDto.toDomain(): Character {
    return Character(
        url = url,
        name = name,
        aliases = aliases,
    )
}
