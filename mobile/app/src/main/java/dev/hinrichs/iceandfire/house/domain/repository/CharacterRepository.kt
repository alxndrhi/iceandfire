package dev.hinrichs.iceandfire.house.domain.repository

import dev.hinrichs.iceandfire.house.domain.Character

interface CharacterRepository {
    suspend fun fetchCharacters(): List<Character>
}
