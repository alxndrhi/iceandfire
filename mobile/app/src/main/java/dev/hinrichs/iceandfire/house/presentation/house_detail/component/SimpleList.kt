package dev.hinrichs.iceandfire.house.presentation.house_detail.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun SimpleList(
    modifier: Modifier = Modifier,
    Headline: String,
    data: List<String>
) {
    Box(modifier = modifier.fillMaxWidth()) {
        Column {
            Text(
                text = Headline,
                style = MaterialTheme.typography.headlineMedium,
            )
            data.forEach {
                Text(
                    modifier = Modifier.padding(top = 15.dp),
                    text = "- $it",
                )
            }
        }
    }
}
