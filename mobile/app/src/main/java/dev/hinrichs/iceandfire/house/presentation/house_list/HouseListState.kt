package dev.hinrichs.iceandfire.house.presentation.house_list

import dev.hinrichs.iceandfire.house.domain.House

data class HouseListState(
    val isLoading: Boolean = false,
    val data: List<House> = emptyList(),
    val error: String = "",
)
