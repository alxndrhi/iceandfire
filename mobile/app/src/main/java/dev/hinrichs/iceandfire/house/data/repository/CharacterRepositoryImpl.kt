package dev.hinrichs.iceandfire.house.data.repository

import dev.hinrichs.iceandfire.house.data.mapper.toDomain
import dev.hinrichs.iceandfire.house.data.remote.IceAndFireApi
import dev.hinrichs.iceandfire.house.domain.Character
import dev.hinrichs.iceandfire.house.domain.repository.CharacterRepository
import javax.inject.Inject

class CharacterRepositoryImpl @Inject constructor(
    private val iceAndFireApi: IceAndFireApi,
) : CharacterRepository {
    override suspend fun fetchCharacters(): List<Character> {
        return iceAndFireApi.fetchCharacters().map { it.toDomain() }
    }
}
