package dev.hinrichs.iceandfire.house.presentation.house_detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import dev.hinrichs.iceandfire.house.presentation.house_detail.component.SimpleList

@Composable
fun HouseDetailScreen(
    viewModel: HouseDetailViewModel = hiltViewModel(),
) {
    val state = viewModel.state.value
    Box(modifier = Modifier.fillMaxSize()) {
        state.data?.let {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                contentPadding = PaddingValues(16.dp)
            ) {
                item {
                    Text(
                        text = state.data.name,
                        style = MaterialTheme.typography.headlineLarge,
                    )
                }
                item {
                    SimpleList(
                        modifier = Modifier.padding(top = 20.dp),
                        Headline = "Ancestral Weapons",
                        data = state.data.ancestralWeapons
                    )
                }
                item {
                    SimpleList(
                        Headline = "Sworn Members",
                        data = state.data.swornMembers
                    )
                }
            }
        }
    }

    if (state.isLoading) {
        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            CircularProgressIndicator(
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }

    if (state.error.isNotBlank()) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(color = Color.DarkGray)
        ) {
            Text(
                text = state.error,
                color = Color.Red,
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(20.dp),
                style = TextStyle(
                    fontWeight = FontWeight.Bold,
                    fontSize = TextUnit(16F, TextUnitType.Sp)
                )
            )
        }
    }
}
