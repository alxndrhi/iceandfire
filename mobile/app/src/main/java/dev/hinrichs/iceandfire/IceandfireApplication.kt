package dev.hinrichs.iceandfire

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class IceandfireApplication : Application()
