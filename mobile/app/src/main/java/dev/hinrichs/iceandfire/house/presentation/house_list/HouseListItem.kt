package dev.hinrichs.iceandfire.house.presentation.house_list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import dev.hinrichs.iceandfire.house.domain.House

@Composable
fun HouseListItem(house: House, onClick: (House) -> Unit) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .clickable { onClick(house) }
        .padding(16.dp),
        horizontalArrangement = SpaceBetween,
    ) {
        Column {
            Text(
                text = house.name,
                style = MaterialTheme.typography.headlineSmall,
            )
            Text(
                text = house.region,
                style = MaterialTheme.typography.bodyMedium,
            )
            Text(
                modifier = Modifier.padding(top = 20.dp),
                text = house.coatOfArms,
                style = MaterialTheme.typography.bodySmall,
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}
