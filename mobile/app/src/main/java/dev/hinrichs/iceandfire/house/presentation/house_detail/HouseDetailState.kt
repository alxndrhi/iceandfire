package dev.hinrichs.iceandfire.house.presentation.house_detail

import dev.hinrichs.iceandfire.house.domain.House

data class HouseDetailState(
    val isLoading: Boolean = false,
    val data: House? = null,
    val error: String = "",
)
