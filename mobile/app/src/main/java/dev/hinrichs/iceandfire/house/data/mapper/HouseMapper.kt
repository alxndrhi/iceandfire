package dev.hinrichs.iceandfire.house.data.mapper

import dev.hinrichs.iceandfire.house.data.remote.HouseDto
import dev.hinrichs.iceandfire.house.domain.House

fun HouseDto.toDomain() = House(
    url = url,
    name = name,
    region = region,
    coatOfArms = coatOfArms,
    words = words,
    titles = titles,
    seats = seats,
    currentLord = currentLord,
    heir = heir,
    overlord = overlord,
    founded = founded,
    founder = founder,
    diedOut = diedOut,
    ancestralWeapons = ancestralWeapons,
    cadetBranches = cadetBranches,
    swornMembers = swornMembers,
)
