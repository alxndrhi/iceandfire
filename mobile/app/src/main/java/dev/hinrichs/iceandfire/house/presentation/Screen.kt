package dev.hinrichs.iceandfire.house.presentation

sealed class Screen(
    val route: String
) {
    object HouseList : Screen("house_list")
    object HouseDetail : Screen("house_detail")
}
